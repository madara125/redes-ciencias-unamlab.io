#	Universidad Nacional Autónoma de México
#	Facultad de Ciencias
#	Ciencias de la Computación

##	Redes de Computadoras

Semestre 2019-2

###	Presentación del curso

+ [Presentación del curso](presentacion.md "Generalidades del curso")

--------------------------------------------------------------------------------

###	Prácticas

+ [Repositorio para la entrega de tareas y prácticas](https://gitlab.com/Redes-Ciencias-UNAM/2019-2/tareas-redes "Las tareas se entregan a traves de merge-request")
+ [Índice de prácticas](http://redes-ciencias-unam.gitlab.io/practicas/ "Índice de prácticas")
+ [Guia rápida del lenguaje C](/public/laboratorio/guia_c.pdf "Guia rápida del lenguaje C")
+ [Especificaciones de las prácticas](/public/laboratorio "Prácticas del laboratorio")

  - [Práctica 1 - 11/02/2019 - Programación en C](/public/laboratorio/practica1 "Práctica 1 - Programación en C")
  - [Práctica 2 - 18/02/2019 - Sockets](/public/laboratorio/practica2 "Práctica 2 - Sockets en C")
  - [Práctica 3 - 25/02/2019 - Manejo de archivos binarios y captura de paquetes](/public/laboratorio/practica3 "Práctica 3 - Manejo de archivos binarios y captura de paquetes")


--------------------------------------------------------------------------------

###	Temas

+ [Carpeta de fotos del pizarrón](http://tinyurl.com/PizarronRedes-2019-2 "Accesible con cuenta @ciencias")
+ [Guía rápida de `git`](temas/git.md "9418/tcp")
+ [Flujo de trabajo para la entrega de tareas y prácticas](https://gitlab.com/Redes-Ciencias-UNAM/workflow#readme "fork y merge request")

####	1 - Capa Física

+ [Medios de transmisión](temas/media.md "Medios de transmisión")

####	2 - Capa de Enlace

+ [Ethernet](temas/ethernet.md "Ethernet")

####	3 - Capa de Red

+ [IP - Internet Protocol](temas/ip.md "Protocolo de Internet")

####	4 - Capa de Transporte

+ [TCP - Transmission Control Protocol](temas/tcp.md "TCP")

####	7 - Capa de Aplicación

+ [WHOIS](temas/whois.md "43/tcp")
+ [DNS - Domain Name System](temas/dns.md "53/udp , 53/tcp")
+ [HTTP - HyperText Transfer Protocol](temas/http.md "80/tcp , 443/tcp")
+ [SSL - Secure Sockets Layer](temas/ssl.md "ssl")
+ [SMTP - Simple Mail Transfer Protocol](temas/smtp.md "25/tcp , 465/tcp , 587/tcp")
+ [DHCP - Dynamic Host Configuration Protocol](temas/dhcp.md "67/udp , 68/udp")

--------------------------------------------------------------------------------

###	Tareas

+ [Repositorio para la entrega de tareas y prácticas](https://gitlab.com/Redes-Ciencias-UNAM/2019-2/tareas-redes "Las tareas se entregan a traves de merge-request")

####	4 - Capa de Transporte

+ [Tarea TCP](tareas/tarea-tcp.md "Tarea sobre el protoclo TCP")

####	7 - Capa de Aplicación

+ [Tarea DNS y WHOIS](tareas/tarea-dns.md "Tarea sobre los protocolos DNS y WHOIS")
+ [Tarea HTTP](tareas/tarea-http.md "Tarea sobre el protocolo HTTP")
+ [Tarea SSL](tareas/tarea-ssl.md "Tarea sobre el protoclo SSL")

--------------------------------------------------------------------------------

###	Ligas de interés

+ <http://redes-ciencias-unam.gitlab.io/>
+ <http://redes-ciencias-unam.gitlab.io/practicas/>
+ <https://gitlab.com/Redes-Ciencias-UNAM/2019-2/tareas-redes>
+ <http://tinyurl.com/ListaRedes-2019-2>
+ <http://tinyurl.com/PizarronRedes-2019-2>
+ <http://www.fciencias.unam.mx/asignaturas/714.pdf>
+ <http://www.fciencias.unam.mx/docencia/horarios/20192/1556/714>
+ <http://www.fciencias.unam.mx/licenciatura/asignaturas/1556/714>
